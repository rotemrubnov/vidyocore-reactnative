package com.vidyocorereactapp;

import android.app.Activity;
import android.content.Intent;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import com.vidyo.vidyocore.VidyoCore;
import com.vidyo.vidyocore.interfaces.OKHTTPResponse;

public class VidyoCoreModule extends ReactContextBaseJavaModule {

    private static ReactApplicationContext reactContext;

    private static final int VIDYO_CALL_REQUEST = 1;
    private static final String E_ACTIVITY_DOES_NOT_EXIST = "E_ACTIVITY_DOES_NOT_EXIST";
    private static final String E_CANCELLED = "E_CANCELLED";
    private static final String E_FAILED = "E_FAILED";
    private static final String E_OK = "E_OK";

    private Promise mVidyoPromise;

    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {

        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
            if (requestCode == VIDYO_CALL_REQUEST) {
                if (mVidyoPromise != null) {
                    if (resultCode == Activity.RESULT_CANCELED) {
                        mVidyoPromise.reject(E_CANCELLED, "Vidyo call cancelled");
                    } else if (resultCode == Activity.RESULT_OK) {
                        mVidyoPromise.resolve(E_OK);
                    } else {
                        mVidyoPromise.resolve(resultCode);
                    }
                }
                mVidyoPromise = null;
            }
        }
    };

    public VidyoCoreModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        reactContext.addActivityEventListener(mActivityEventListener);
    }

    @Override
    public String getName() {
        return "VidyoCore";
    }

    @ReactMethod
    public void startVidyoCall(Integer appID, Integer callCode, final Promise promise) {
        final Activity currentActivity = getCurrentActivity();
        if (currentActivity == null) {
            promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist");
        }
        mVidyoPromise = promise;

        VidyoCore.newVidyoIntent(this.reactContext, appID, callCode, new OKHTTPResponse() {
            @Override
            public void onResponse(Intent intent) {
                currentActivity.startActivityForResult(intent, VIDYO_CALL_REQUEST);
            }

            @Override
            public void onFailure(String s) {
                mVidyoPromise.reject(E_FAILED, s);
            }
        });

        }
}
