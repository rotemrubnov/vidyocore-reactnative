# VidyoCore React Native

This sample React Native app integrates the VidyoCore Android library and shows example usage, i.e. starting a Vidyo Call, in Javascript.

## Requirements: 
- React native CLI
```
npm install -g react-native-cli
```

## How to run this sample project: 

- Clone this project
- `cd vidyocore-reactnative`
- open `App.js` and edit the code section below, replacing the two integer values with your AppID and callCode respectively. These values are required in order to start a Vidyo call:
```javascript
  async function startCall() {
    try{
      var result = await VidyoCore.startVidyoCall(685, 26063) // <-- appID and callCode
      console.log('Call completed: ' + result)
    }
    catch(e) {
      console.log(e)
    }
  }
```
- Connect an Android device via USB, ensure USB-debugging is enabled on the device.
- run `react-native run-android`

## How to add VidyoCore to your ReactNative App:

- Open the `./android` folder of your ReactNative project in Android Studio
- Click right on your project. Press on Open **Module Settings**
- Press on the + on the Left-top corner.
- Press on **import a .jar/.aar file**.
- Select the .aar file, and add it.
- Still in Module Settings, select your project, go to dependencies and press the plus -> **module dependency**, and select the module you just created. 

## Usage Native Code (Java) 

### VidyoCoreModule.java

Create a new module to wrap the VidyoCore functionality and expose to Javascript. The following example defines `startVidyoCall` method using a `Promise`. The promise is optional. Only the `appID` and `callCode` params are actually required in order to join a vidyo call.

```java
public class VidyoCoreModule extends ReactContextBaseJavaModule {

    private static ReactApplicationContext reactContext;

    private static final int VIDYO_CALL_REQUEST = 1;
    private static final String E_ACTIVITY_DOES_NOT_EXIST = "E_ACTIVITY_DOES_NOT_EXIST";
    private static final String E_CANCELLED = "E_CANCELLED";
    private static final String E_FAILED = "E_FAILED";
    private static final String E_OK = "E_OK";

    private Promise mVidyoPromise;

    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {

        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
            if (requestCode == VIDYO_CALL_REQUEST) {
                if (mVidyoPromise != null) {
                    if (resultCode == Activity.RESULT_CANCELED) {
                        mVidyoPromise.reject(E_CANCELLED, "Vidyo call cancelled");
                    } else if (resultCode == Activity.RESULT_OK) {
                        mVidyoPromise.resolve(E_OK);
                    } else {
                        mVidyoPromise.resolve(resultCode);
                    }
                }
                mVidyoPromise = null;
            }
        }
    };

    public VidyoCoreModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        reactContext.addActivityEventListener(mActivityEventListener);
    }

    @Override
    public String getName() {
        return "VidyoCore";
    }

    @ReactMethod
    public void startVidyoCall(Integer appID, Integer callCode, final Promise promise) {
        final Activity currentActivity = getCurrentActivity();
        if (currentActivity == null) {
            promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist");
        }
        mVidyoPromise = promise;

        VidyoCore.newVidyoIntent(this.reactContext, appID, callCode, new OKHTTPResponse() {
            @Override
            public void onResponse(Intent intent) {
                currentActivity.startActivityForResult(intent, VIDYO_CALL_REQUEST);
            }

            @Override
            public void onFailure(String s) {
                mVidyoPromise.reject(E_FAILED, s);
            }
        });

        }
}
```

### VidyoCorePackage.java

Create a Package to be exported to Javascript:

```java
public class VidyoCorePackage implements ReactPackage {
    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        return Arrays.<NativeModule>asList(new VidyoCoreModule(reactContext));
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }

}
``` 

### MainApplication.java

Modify the Application class to extend from `VidyoCore`:

```java
import com.vidyo.vidyocore.VidyoCore;
...
public class MainApplication extends VidyoCore implements ReactApplication {
      new ReactNativeHost(this) {
        ...
        @Override
        protected List<ReactPackage> getPackages() {
          @SuppressWarnings("UnnecessaryLocalVariable")
          List<ReactPackage> packages = new PackageList(this).getPackages();
          // Packages that cannot be autolinked yet can be added manually here, for example:
           packages.add(new VidyoCorePackage());
          return packages;
        }
        ...
      };
    
}
```

## Reference

This sample was created based on [ReactNative docs](https://reactnative.dev/docs/native-modules-android)



